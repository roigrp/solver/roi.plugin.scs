PKG=ROI.plugin.scs

readme:
	R -e 'rmarkdown::render("README.Rmd", clean = FALSE)'
		
build:
	R CMD build .

inst: build
	R CMD INSTALL ${PKG}*.tar.gz
	
check: build
	R CMD check ${PKG}*.tar.gz

manual: clean
	R CMD Rd2pdf --output=Manual.pdf .

clean:
	rm -f Manual.pdf README.knit.md README.html ${PKG}_*.tar.gz
	rm -rf .Rd2pdf*
	rm -rf ${PKG}.Rcheck


check_mac_m1: build
	R -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'macos-m1-bigsur-release')"

check_mac_old: build
	R -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'macos-highsierra-release-cran')"

check_gcc_san: build
	R -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'linux-x86_64-rocker-gcc-san')"

check_debian_clang: build
	R -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'debian-clang-devel')"

devcheck_win_devel: build
	R -e "devtools::check_win_devel(email = 'FlorianSchwendinger@gmx.at')"
